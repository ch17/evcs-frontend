import user from './Auth'
 
export default {
    guest (to, from, next) {
        next(!user.check())
    },
 
    auth (to, from, next) {
        next(user.check() ? true : {
            path: '/companies',
            query: {
                redirect: to.name
            }  
        })
    }
}