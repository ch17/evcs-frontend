import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from 'axios';
import Guard from './services/Middleware';


import Companies from './Companies.vue';
import Stations from './Stations.vue';
import AddStations from './AddStations.vue';
import Home from './Home.vue';
import Login from './Login.vue';
import Logout from './Logout.vue';

Vue.use(VueRouter);
Vue.use(axios);


const routes = [
        {
          path: '/',
          name: 'Home',
          component: Home,
          beforeEnter: Guard.guest
        },

        {
          path: '/login',
          name: 'Login',
          component: Login,
          beforeEnter: Guard.guest
        },
        
        {
          path: '/companies',
          name: 'Companies',
          component: Companies,
          beforeEnter: Guard.auth
        },
        
        {
          path: '/stations',
          name: 'Stations',
          component: Stations,
          beforeEnter: Guard.auth
        },

        {
          path: '/stations/:company_id',
          name: 'CompanyStations',
          component: Stations,
          beforeEnter: Guard.auth
        },

        {
          path: '/stations/add',
          name: 'AddStations',
          component: AddStations,
          beforeEnter: Guard.auth
        },

        {
          path: '/logout',
          name: 'Logout',
          component: Logout,
          beforeEnter: Guard.auth
        }

];

const router = new VueRouter({
  routes
});

new Vue({
  el: '#app',
  router
})
